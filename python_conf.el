(message (concat "Loading python config from: " emacs-init-dir "python_conf.el"))

(setq python-indent-offset 4)

(when (require 'elpy nil 'noerror)
  (setenv "WORKON_HOME" (concat (getenv "HOME")"/anaconda3/envs"))
  (elpy-enable)
  ;; (add-to-list 'company-backends 'elpy-company-backend) ;; https://github.com/jorgenschaefer/elpy/issues/1101
  (setq elpy-rpc-python-command "python3")
  ;; (el-get-envpath-prepend "PYTHONPATH" default-directory)
  (setq python-shell-prompt-detect-failure-warning nil)
  (setq elpy-rpc-virtualenv-path 'current)
  )


(defun python-clear ()
  "Clears the shell buffer ala Unix's clear or DOS' cls"
  (interactive)
  ;; the shell prompts are read-only, so clear that for the duration
  (let ((inhibit-read-only t))
    ;; simply delete the region
    (delete-region (point-min) (point-max)))
  (comint-send-input))


(defun my-python-hook ()
  (local-set-key "\C-l" 'python-clear))

(add-hook 'inferior-python-mode-hook 'my-python-hook)

(add-hook 'jupyter-repl-mode-hook 'my-python-hook)
;; Hotfix
(add-hook 'jupyter-repl-mode-hook (lambda () (font-lock-mode 0)))

(defun my-python-set-shell (proc)
  (interactive (ob-ipython--choose-kernel))
  (when proc
    (setq-local python-shell-buffer-name
                (format "Python:%s" (car (last (split-string (process-name proc) "-")))))))
