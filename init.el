;; This is the first thing to get loaded.
;;
(require 'cl-lib) ; a rare necessary use of REQUIRE, this is for common lisp!
(defvar *emacs-load-start* (current-time))

;; remember this directory
(setq emacs-init-dir
      (file-name-directory (or load-file-name (buffer-file-name))))

;; ========== Essential settings ==========
(setq inhibit-startap-message t)
(setq initial-scratch-message "")
(setq inhibit-startup-screen t)
(blink-cursor-mode t)
(setq-default fill-column 66)                 ;; Make newline insertion in the right place
(column-number-mode 1)                        ;; Show column-number in the mode line
(scroll-bar-mode -1)                          ;; Removes the scroll bar
(tool-bar-mode -1)                            ;; Hide toolbar in graphics mode
(menu-bar-mode -1)                            ;; Remove the menu bar, Remove this when menu becomes part of the global menu.
(global-set-key [f1] 'menu-bar-mode)          ;; Toggle menu bar mode with f1
(setq-default truncate-lines t)               ;; Do not wrapp lines by default
(global-set-key [f12] 'toggle-truncate-lines) ;; Toggle-truncate-lines keybinding
(global-set-key [f11] 'visual-line-mode)      ;; Toggle nice soft line wrapping
(setq blink-matching-delay .1)                ;; Matching parenthesis timer default 1
(setq-default indent-tabs-mode nil)           ;; Make sure no tabs are present.
(setq-default tab-width 4)
(delete-selection-mode t)                     ;; Make it so that selected text are removed on new entries
(setq ring-bell-function 'ignore)             ;; Just ignore all the annoying blips and bloops. 

(require 'package)
(setq package-user-dir (concat emacs-init-dir "elpa"))
(add-to-list 'package-archives
             '("melpa" . "http://melpa.org/packages/")
             '("org" . "http://orgmode.org/elpa/"))
;; (package-refresh-contents)
(package-initialize)

;; (add-to-list 'el-get-recipe-path (concat emacs-init-dir "recipes"))
(if (file-exists-p (concat emacs-init-dir "user.el"))
    (load-file (concat emacs-init-dir "user.el"))
  (message (concat "user info can be placed in: " emacs-init-dir "user.el")))


;; Load paths
(add-to-list 'load-path (concat emacs-init-dir "lisp"))
(setq autoload-file (concat emacs-init-dir "loaddefs.el"))
(setq custom-file "/dev/null")
(defun load-if-exists (f)
  (if (file-readable-p f)
      (load-file f)))
(defmacro with-library (symbol &rest body)
  `(condition-case nil
       (progn
         (require ',symbol)
         ,@body)

     (error (message (format "I guess we don't have %s available." ',symbol))
            nil)))

;; Ubiquitous Packages which should be loaded on startup rather than autoloaded on demand since they are likely to be used in every session.
(require 'saveplace)
(require 'ffap)
(require 'uniquify)
(require 'ansi-color)
(require 'calendar)
(require 'recentf) ; This or
;; (recentf-mode 1) ; this
(setq recentf-save-file (concat emacs-init-dir "recentf"))

(message "Loading emacs init")

(when (file-exists-p (concat emacs-init-dir "config.el"))
  (message (concat "Loading general configuration from: " emacs-init-dir "config.el"))
  (load-file (concat emacs-init-dir "config.el")))

(message "init.el has loaded in %d s" (cl-destructuring-bind (hi lo ms ps) (current-time) (- (+ hi lo) (+ (cl-first *emacs-load-start*) (cl-second *emacs-load-start*)))))
;;; init.el ends here
