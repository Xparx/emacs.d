(message (concat "inside: " emacs-init-dir "config.el"))

;; key-bindings
(when (file-exists-p (concat emacs-init-dir "bindings_conf.el"))
  (load-file (concat emacs-init-dir "bindings_conf.el")))


;; Magit
(when (require 'magit nil 'noerror)
  (message "loading magit")
  (global-set-key (kbd "C-c m") 'magit-status)
  (setq magit-status-buffer-switch-function 'switch-to-buffer)
  (setq magit-diff-refine-hunk 'all)

  (defun *magit-delete-trailing-whitespace-from-file ()
    "Removes whitespace from the current item."
    (interactive)
    (save-excursion
      (magit-diff-visit-file-worktree (magit-file-at-point))
      (delete-trailing-whitespace)
      (save-buffer)
      (kill-buffer))
    (magit-refresh))

  (eval-after-load 'magit
    '(define-key magit-status-mode-map (kbd "w") '*magit-delete-trailing-whitespace-from-file))
  )


;; Not sure if needed.
;; (when (require 'exec-path-from-shell nil 'noerror)
;;   (exec-path-from-shell-initialize))


;; Undo-tree source: https://elpa.gnu.org/packages/undo-tree.html,
;; http://pragmaticemacs.com/emacs/advanced-undoredo-with-undo-tree
(when (require 'undo-tree nil 'noerror)
  (global-undo-tree-mode 1)
  (setq undo-tree-auto-save-history nil)
  ;; make ctrl-z undo
  (global-set-key (kbd "C-z") 'undo)
  ;; make ctrl-Z redo
  (defalias 'redo 'undo-tree-redo)
  (global-set-key (kbd "C-S-z") 'redo)
  (add-hook 'eshell-mode-hook (lambda () (undo-tree-mode 0)))
  )


(when (file-exists-p (concat emacs-init-dir "misc.el"))
  (load-file (concat emacs-init-dir "misc.el")))


;; Powerline
(when (require 'powerline nil 'noerror)
  (powerline-default-theme)
  (setq powerline-default-separator 'arrow)
  (set-face-attribute 'mode-line nil
                      :foreground "white"
                      :background "goldenrod"
                      :box nil)
  (set-face-attribute 'mode-line-inactive nil
                      :foreground "grey80"
                      :background "grey60"
                      :box nil))


;; Company-mode
(when (require 'company-mode nil 'noerror)
  (add-hook 'after-init-hook 'global-company-mode)
  )


;; LaTeX
(when (file-exists-p (concat emacs-init-dir "latex_conf.el"))
  (load-file (concat emacs-init-dir "latex_conf.el")))


;; Visual regex
(when (require 'visual-regexp-steroids nil 'noerror)
  (global-set-key (kbd "C-c r") 'vr/replace)
  (global-set-key (kbd "M-%") 'vr/query-replace)
  ;; (global-set-key (kbd "C-s") 'vr/isearch-forward)
  ;; (global-set-key (kbd "C-r") 'vr/isearch-backward)
  (define-key esc-map (kbd "C-r") 'vr/isearch-backward) ;; C-M-r
  (define-key esc-map (kbd "C-s") 'vr/isearch-forward) ;; C-M-s

  ;;   ;; and make sure it works the same way as native isearch when searching
  ;;   (defadvice vr--isearch (around add-case-insensitive (forward string &optional bound noerror count) activate)
  ;;     (when (and (eq vr/engine 'python) case-fold-search)
  ;;       (setq string (concat "(?i)" string)))
  ;;     ad-do-it)

  ;; Disable visual-regexp in pdfview
  (add-hook 'pdf-view-mode-hook
            (lambda ()
              (progn
                (local-set-key (kbd "C-s") 'isearch-forward)
                (local-set-key (kbd "C-r") 'isearch-backwards)))))


;;   ;; Disable visual-regexp in minibuffer
;;   (define-key minibuffer-local-map (kbd "C-s") 'isearch-forward)
;;   (define-key minibuffer-local-map (kbd "C-r") 'isearch-backwards)
;;   )


;; ace-windows, https://github.com/abo-abo/ace-window
(when (require 'ace-window nil 'noerror)
  (global-set-key (kbd "C-x o") 'ace-window)
  (setq aw-background nil aw-keys '(?a ?s ?d ?f ?g ?h ?j ?k ?l)))


;; orgmode
(when (file-exists-p (concat emacs-init-dir "org_conf.el"))
  (load-file (concat emacs-init-dir "org_conf.el")))

(when (file-exists-p (concat emacs-init-dir "coding_conf.el"))
  (load-file (concat emacs-init-dir "coding_conf.el")))

;; yasnippet
(when (require 'yasnippet nil 'noerror)
  (yas-global-mode 1)
  (add-hook 'org-mode-hook
            #'(lambda ()
               (yas-minor-mode)))
  ;; Remap =yas-expand= for relevant modes as it over shadows the otherwise very nice autocompletion. https://joaotavora.github.io/yasnippet/snippet-expansion.html
  (define-key yas-minor-mode-map (kbd "TAB") nil)
  (define-key yas-minor-mode-map (kbd "<tab>") nil)
  (define-key yas-minor-mode-map (kbd "C-TAB") 'yas-expand)
  (define-key yas-minor-mode-map (kbd "<C-tab>") 'yas-expand)
  (define-key yas-minor-mode-map (kbd "C-1") 'yas-expand)
  (define-key yas-minor-mode-map (kbd "<C-1>") 'yas-expand)
  )

