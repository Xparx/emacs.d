(message (concat "Loading key bindings : " emacs-init-dir "bindings_conf.el"))


(global-set-key (kbd "C-x \\") 'align-regexp)
(global-set-key (kbd "C-c w") 'whitespace-cleanup)
(global-set-key (kbd "M-/") 'hippie-expand)

(define-key global-map (kbd "C-+") 'text-scale-increase)
(define-key global-map (kbd "C--") 'text-scale-decrease)

(global-set-key (kbd "C-s") 'isearch-forward-regexp)
(global-set-key (kbd "C-r") 'isearch-backward-regexp)
(global-set-key (kbd "C-M-s") 'isearch-forward)
(global-set-key (kbd "C-M-r") 'isearch-backward)

;; (global-set-key (kbd "C-x M-f") 'ido-find-file-other-window)
;; (global-set-key (kbd "C-x C-M-f") 'find-file-in-project)
;; (global-set-key (kbd "C-x C-p") 'find-file-at-point)
;; (global-set-key (kbd "C-c y") 'bury-buffer)
;; (global-set-key (kbd "C-c M-r") 'revert-buffer)
;; (global-set-key (kbd "M-`") 'file-cache-minibuffer-complete)
(global-set-key (kbd "C-x C-b") 'ibuffer)

(global-set-key (kbd "C-<prior>") 'previous-buffer) ; Ctrl+PageDown
(global-set-key (kbd "C-<next>") 'next-buffer) ; Ctrl+PageUp

(global-set-key (kbd "C-h a") 'apropos)

;; Rgrep is infinitely useful in multi-file projects.
;; see: (describe-function 'rgrep)
(define-key global-map "\C-x\C-r" 'rgrep)


;; cycle through amounts of spacing
;; http://pragmaticemacs.com/emacs/cycle-spacing
(global-set-key (kbd "M-SPC") 'cycle-spacing)

;; The Org-mode agenda is good to have close at hand, http://orgmode.org/manual/Agenda-Views.html#Agenda-Views
(define-key global-map "\C-ca" 'org-agenda)

;; Org-mode supports; http://orgmode.org/manual/Hyperlinks.html#Hyperlinks, this command allows you to store links globally for later insertion into an Org-mode buffer. See: http://orgmode.org/manual/Handling-links.html#Handling-links in the Org-mode manual.
(define-key global-map "\C-cl" 'org-store-link)

(defalias 'yes-or-no-p 'y-or-n-p)
(defalias 'rs 'replace-string)
