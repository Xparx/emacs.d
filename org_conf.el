(message (concat "Loading orgmode config from: " emacs-init-dir "org_conf.el"))

(setq initial-scratch-message "\n#+begin_src emacs-lisp\n\n#+end_src\n\n")
(setq initial-major-mode 'org-mode)
(setq org-ellipsis " ⬎")
(setq org-catch-invisible-edits 'show)
(setq org-hide-leading-stars t)
(setq org-confirm-babel-evaluate nil)
(setq org-export-babel-evaluate nil)
(setq org-src-fontify-natively t)
(setq org-src-tab-acts-natively t)
(setq org-edit-src-content-indentation 0)
(setq org-src-window-setup 'current-window)
(setq org-highlight-latex-and-related '(latex script entities))
(setq org-use-sub-superscripts '{})
(setq org-pretty-entities t)

;; (add-to-list 'auto-mode-alist '("\\.org$" . org-mode))

(add-hook 'org-mode-hook
          (lambda()
            (local-unset-key (kbd "C-c ;"))))


;; org-pdfview: https://emacs.stackexchange.com/a/24502/427
;; (when (require 'org-pdfview nil 'noerror)
;; (when  (eval-after-load 'org '(require 'org-pdfview nil 'noerror))
;;   (add-to-list 'org-file-apps
;;                '("\\.pdf\\'" . (lambda (file link)
;;                                  (org-pdfview-open link)))))

(setq org-export-allow-bind-keywords t)

;; Make attr_* :widht XXXpx work for inline images
(setq org-image-actual-width nil)

;; (add-to-list 'org-structure-template-alist '("p" "#+LATEX_HEADER: \\usepackage{}"))

;; Indent org-mode correctly https://stackoverflow.com/questions/1771981/how-to-keep-indentation-with-emacs-org-mode-visual-line-mode
(setq org-startup-indented t)
;; and with correct levels
(setq org-indent-indentation-per-level 1)

(setq org-directory "~/notebook")

;; Babel languages
(setq org-babel-load-languages '((emacs-lisp . t)
                                 (sql . t)
                                 (shell . t)
                                 (matlab . t)
                                 (octave . t)
                                 (gnuplot . t)
                                 (python . t)
                                 (dot . t)
                                 (ditaa . t)
                                 (latex . t)
                                 (js . t)
                                 (R . t)
                                 (C . t)
                                 (css . t)
                                 ;; (calc . t)
                                 (maxima . t)
                                 (perl . t)))

(when (executable-find "julia")
  (add-to-list 'org-babel-load-languages '(julia . t)))

(when (require 'jupyter nil 'noerror)
  (org-babel-jupyter-override-src-block "python")
  (add-to-list 'org-babel-load-languages '(jupyter . t)))

(org-babel-do-load-languages 'org-babel-load-languages org-babel-load-languages)

;; lob
;; (org-babel-lob-ingest (expand-file-name "org-mode/doc/library-of-babel.org" el-get-dir)) ;; what to put here?
;; (org-babel-lob-ingest (expand-file-name "org/personal-lob.org" emacs-init-dir)) ;; directory needs to exist.


;; org export
;; html5 fancy: https://emacs.stackexchange.com/a/27697/427
(setq org-export-with-sub-superscripts '{}
      org-html-html5-fancy t
      org-html-doctype "html5")
(setq org-export-copy-to-kill-ring nil)
(setq org-latex-prefer-user-labels t)
(setq org-export-coding-system 'utf-8)
(setq org-html-validation-link nil)


;; Make org understand latex syntax: http://stackoverflow.com/questions/11646880/flyspell-in-org-mode-recognize-latex-syntax-like-auctex
(add-hook 'org-mode-hook (lambda () (setq ispell-parser 'tex)))

;; make ispell treat src blocks ok. http://endlessparentheses.com/ispell-and-org-mode.html
(defun endless/org-ispell ()
  "Configure `ispell-skip-region-alist' for `org-mode'."
  (make-local-variable 'ispell-skip-region-alist)
  (add-to-list 'ispell-skip-region-alist '(org-property-drawer-re))
  (add-to-list 'ispell-skip-region-alist '("~" "~"))
  (add-to-list 'ispell-skip-region-alist '("=" "="))
  (add-to-list 'ispell-skip-region-alist '("^ *#\\+BEGIN_SRC" . "^ *#\\+END_SRC"))
  (add-to-list 'ispell-skip-region-alist '("^ *#\\+begin_src" . "^ *#\\+end_src")))
(add-hook 'org-mode-hook #'endless/org-ispell)

;; Org-mode hook for tex-master
(add-hook 'org-mode-hook (setq TeX-master t))

;; http://pragmaticemacs.com/emacs/prevent-comments-from-breaking-paragraphs-in-org-mode-latex-export
;; https://emacs.stackexchange.com/questions/22574/orgmode-export-how-to-prevent-a-new-line-for-comment-lines
;; remove comments from org document for use with export hook
(defun delete-org-comments (backend)
  (cl-loop for comment in (reverse (org-element-map (org-element-parse-buffer)
                    'comment 'identity))
    do
    (setf (buffer-substring (org-element-property :begin comment)
                (org-element-property :end comment))
          "")))

;; add to export hook
(add-hook 'org-export-before-processing-hook 'delete-org-comments)

(add-to-list 'org-latex-packages-alist
             '("" "easySymbols" t))

;; Math snippets
(when (executable-find "dvisvgm")
  (setq org-preview-latex-default-process 'dvisvgm))
;; Scale math snippets to a reasonable size (it seems like the correct way of doing this is found in the variable [[help:org-format-latex-options]])
(plist-put (alist-get 'dvipng org-preview-latex-process-alist)
           :image-size-adjust '(1.5 . 1.5))
(plist-put (alist-get 'dvisvgm org-preview-latex-process-alist)
           :image-size-adjust '(2.2 . 2.2))

(defun my-orgmode-cheat-sheet ()
  (interactive)
  (eww "http://orgmode.org/orgcard.txt"))
