(message (concat "Loading conding config from: " emacs-init-dir "coding_conf.el"))

;; R
(when (featurep 'ess)
  (load "ess-autoloads")
  (add-hook 'R-mode-hook (lambda () (ess-force-buffer-current)))
  (add-to-list 'auto-mode-alist '("\\.R" . R-mode))
  (add-hook 'R-mode-hook (lambda () (message "Associate with an R process with C-c C-s")))
  (setq ess-tab-complete-in-script t)
  )

;; julia
(when (and (featurep 'ess) (executable-find "julia"))
  (setq inferior-julia-program-name (executable-find "julia"))
  (add-to-list 'auto-mode-alist '(".juliahistory\\'" . julia-mode))
  (defun my-julia-hook ()
    (local-set-key "\C-l" 'inferior-clear))
  (add-hook 'julia-post-run-hook 'my-julia-hook)

  (defun my-julia-mode-hook ()
    (setq company-mode t)
    (setq auto-complete-mode t))
  (add-hook 'julia-mode-hook 'my-julia-mode-hook)
  )

;; gnuplot-mode
;; (when (require 'gnuplot-mode nil 'noerror)
(setq auto-mode-alist
      (append '(("\\.gp$" . gnuplot-mode))
              '(("\\.gnuplot$" . gnuplot-mode))
              '(("\\.plt$" . gnuplot-mode))
              '(("\\.gnup$" . gnuplot-mode))
              '(("\\.pal$" . gnuplot-mode))
              '(("\\.plt$" . gnuplot-mode))
              auto-mode-alist))


;; Python
(when (file-exists-p (concat emacs-init-dir "python_conf.el"))
  (load-file (concat emacs-init-dir "python_conf.el")))

;; Eshell
;; https://www.masteringemacs.org/article/complete-guide-mastering-eshell
(setq eshell-directory-name (concat emacs-init-dir "eshell"))
;; https://emacs.stackexchange.com/questions/14522/how-do-i-change-behaviour-of-eshell-rm-rm-in-eshell-tab-completion
(require 'pcmpl-unix)
(defun pcomplete/rm ()
  "Completion for `rm'."
  (let* (
         (pcomplete-file-ignore nil)
         (pcomplete-dir-ignore "^../$\\|^./$")
         (pcomplete-help "(fileutils)rm invocation"))
    (pcomplete-opt "dfirRv")
    (while (pcomplete-here (pcomplete-entries) nil 'expand-file-name))))


;; Ansi-term
;; A mix of http://oremacs.com/2015/01/01/three-ansi-term-tips and a little of https://echosa.github.io/blog/2012/06/06/improving-ansi-term
;; kill buffer after terminating shell
(defun oleh-term-exec-hook ()
  (let* ((buff (current-buffer))
         (proc (get-buffer-process buff)))
    (set-process-sentinel
     proc
     `(lambda (process event)
        (if (string= event "finished\n")
            (kill-buffer ,buff))))))

(add-hook 'term-exec-hook 'oleh-term-exec-hook)
(setq explicit-shell-file-name (executable-find "bash"))

(defun my-term-use-utf8 ()
  (set-buffer-process-coding-system 'utf-8-unix 'utf-8-unix))
(add-hook 'term-exec-hook 'my-term-use-utf8)

(defun my-term-hook ()
  (goto-address-mode))
(add-hook 'term-mode-hook 'my-term-hook)

(eval-after-load "term"
  '(define-key term-raw-map (kbd "C-c C-y") 'term-paste))
