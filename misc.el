(message (concat "Loading miscellaneous from: " emacs-init-dir "misc.el"))

;; Calendar config
(calendar-set-date-style 'iso)
(setq calendar-week-start-day 1)

;; TAB completion
(add-to-list 'completion-styles 'initials t)
(add-hook 'emacs-lisp-mode-hook (lambda () (setq tab-always-indent 'complete)))
(add-hook 'sh-mode-hook (lambda () (setq tab-always-indent 'complete)))


;; Don't clutter up directories with files~ or #files#
(setq backup-directory-alist `(("." . ,(expand-file-name
                                        (concat emacs-init-dir "backups")))))
(setq version-control t        ;; Use version numbers for backups.
      kept-new-versions 2      ;; Number of newest versions to keep (default)
      kept-old-versions 1      ;; Number of oldest versions to keep.
      delete-old-versions t)   ;; Don't ask to delete excess backup versions.

;; Make colors work in M-x shell
(add-hook 'shell-mode-hook 'ansi-color-for-comint-mode-on)


;; Line wrapping
(add-hook 'LaTeX-mode-hook #'visual-line-mode)
(add-hook 'org-mode-hook #'visual-line-mode)
(add-hook 'python-mode-hook  #'visual-line-mode)


;; Spelling (aspell default)
(setq-default ispell-program-name (executable-find "aspell"))

(when (string= ispell-program-name (executable-find "aspell"))
  (setq ispell-list-command "--list")
  (setq flyspell-issue-welcome-flag nil)
  (setq flyspell-issue-message-flag nil))

(setq ispell-dictionary "english") ;; default dict
(setq ispell-silently-savep t) ;; Save to personal dictionary quietly


;; open files
(add-to-list 'auto-mode-alist '(".bashrc\\'" . shell-script-mode))
(add-to-list 'auto-mode-alist '(".bash_variables\\'" . shell-script-mode))
(add-to-list 'auto-mode-alist '(".bash_aliases\\'" . shell-script-mode))

(auto-compression-mode t)
(recentf-mode 1)


;; clear buffers with C-l
;; source: https://stackoverflow.com/questions/7733668/command-to-clear-shell-while-using-emacs-shell
(defun inferior-clear ()
  (interactive)
  (let ((comint-buffer-maximum-size 0))
    (comint-truncate-buffer)))
(defun my-shell-hook ()
  (local-set-key "\C-l" 'inferior-clear))

(add-hook 'shell-mode-hook 'my-shell-hook)

;; source: https://www.linuxquestions.org/questions/programming-9/emacs-eshell-how-to-clear-screen-770328
(defun eshell-clear ()
  "Clears the shell buffer ala Unix's clear or DOS' cls"
  (interactive)
  ;; the shell prompts are read-only, so clear that for the duration
  (let ((inhibit-read-only t))
    ;; simply delete the region
    (delete-region (point-min) (point-max)))
  (eshell-send-input))
(defun my-eshell-hook ()
  (local-set-key "\C-l" 'eshell/clear))

(add-hook 'eshell-mode-hook 'my-eshell-hook)

;; Save last place in visited files
(setq save-place-file (concat emacs-init-dir "saved-places"))
(setq-default save-place t)


;;
(set-face-background 'show-paren-match (face-background 'default))
(set-face-foreground 'show-paren-match "#def")
(set-face-attribute 'show-paren-match nil :weight 'extra-bold)


;; Tramp (not sure how this works.)
(setq tramp-default-method "ssh")
(set-default 'tramp-default-proxies-alist (quote ((".*" "\\`root\\'" "/ssh:%h:"))))


;;;;;; Dired mode ;;;;;;;
(require 'dired-aux)

;; When in dired mode 'a' will find alternative file/dir in the same buffer.
;; http://emacsblog.org/2007/02/25/quick-tip-reuse-dired-buffers.
(put 'dired-find-alternate-file 'disabled nil)

;; Activating dired-x
;; http://www.masteringemacs.org/articles/2014/04/10/dired-shell-commands-find-xargs-replacement
(add-hook 'dired-mode-hook
          (lambda ()
            (setq truncate-lines t)
            (setq dired-x-hands-off-my-keys nil) ;; Remap my keys so C-x C-f finds file at point
            (load "dired-x")
            (dired-hide-details-mode)))

;; Default format of dired (=s= sorts the list based on date)
;; http://ergoemacs.org/emacs/dired_sort.html
(setq dired-listing-switches "-ao -h --group-directories-first --time-style long-iso")

(global-set-key (kbd "C-x C-d") 'dired)
(global-set-key (kbd "C-x d") 'list-directory)


;; Guess program by file extension (http://oremacs.com/2015/01/04/dired-nohup)
(setq dired-guess-shell-alist-user
      '(("\\.pdf\\'" "evince" "okular")
        ("\\.\\(?:cbr\\|cbz\\)\\'" "evince")
        ("\\.\\(?:djvu\\|eps\\)\\'" "evince")
        ("\\.\\(?:jpg\\|jpeg\\|png\\|gif\\|xpm\\)\\'" "eog")
        ("\\.\\(?:xcf\\)\\'" "gimp")
        ("\\.ods\\'\\|\\.xlsx?\\'\\|\\.docx?\\'\\|\\.csv\\'" "libreoffice")
        ("\\.tex\\'" "pdflatex" "latex")
        ("\\.\\(?:mp4\\|mkv\\|avi\\|flv\\|ogv\\|rar\\)\\(?:\\.part\\)?\\'" "vlc")
        ("\\.\\(?:mp3\\|flac\\)\\'" "rhythmbox")
        ("\\.html?\\'" "chromium" "webbrowser-app" "firefox")
        ("\\.cue?\\'" "audacious")))


(defvar dired-filelist-cmd
  '(("vlc" "-L")))

(defun dired-start-process (cmd &optional file-list)
  (interactive
   (let ((files (dired-get-marked-files
                 t current-prefix-arg)))
     (list
      (dired-read-shell-command "& on %s: "
                                current-prefix-arg files)
      files)))
  (let (list-switch)
    (start-process
     cmd nil shell-file-name
     shell-command-switch
     (format
      "nohup 1>/dev/null 2>/dev/null %s \"%s\""
      (if (and (> (length file-list) 1)
               (setq list-switch
                     (cadr (assoc cmd dired-filelist-cmd))))
          (format "%s %s" cmd list-switch)
        cmd)
      (mapconcat #'expand-file-name file-list "\" \"")))))

(define-key dired-mode-map "r" 'dired-start-process)

;;;;;;;;;;;;;;;;;;;


;; kill client buffer with C-x k http://www.emacswiki.org/emacs/EmacsClient
(add-hook 'server-switch-hook
          (lambda ()
            (when (current-local-map)
              (use-local-map (copy-keymap (current-local-map))))
            (when server-buffer-clients
              (local-set-key (kbd "C-x k") 'server-edit))))


;; Make sure copying from desktop works better (Warning: This might get memory intensive)
;; Make sure the desktop copy gets saved in kill-ring even though something else is cut before.
(setq save-interprogram-paste-before-kill t)

;; Ibuffer settings, source: http://www.emacswiki.org/emacs/IbufferMode. Lots of interesting stuff in there.
(setq ibuffer-saved-filter-groups
      (quote (("default"
               ("org-mode" (mode . org-mode))
               ("MATLAB" (mode . matlab-mode))
               ("LaTeX" (or
                         (mode . latex-mode)
                         (mode . bibtex-mode)))
               ("python" (or
                         (mode . python-mode)
                         (mode . inferior-python-mode)
                         (name . "^\\*ob-ipython-client-driver\\*$")
                         (name . "^\\*ob-ipython-kernel\*")))
               ("planner" (or
                           (name . "^\\*Calendar\\*$")
                           (name . "^diary$")))
               ("Emacs" (or
                         (name . "^\\*scratch\\*$")
                         (name . "^\\*ielm\\*$")
                         (name . "^\\*Completions\\*$")
                         (name . "^\\*Messages\\*$")))
               ("Magit" (name . "^\\*magit.*\\*$"))
               ("dired" (mode . dired-mode))
               ("gnus" (or
                        (mode . message-mode)
                        (mode . bbdb-mode)
                        (mode . mail-mode)
                        (mode . gnus-group-mode)
                        (mode . gnus-summary-mode)
                        (mode . gnus-article-mode)
                        (name . "^\\.bbdb$")
                        (name . "^\\.newsrc-dribble")))))))

(add-hook 'ibuffer-mode-hook
          (lambda ()
            (ibuffer-switch-to-saved-filter-groups "default")))


;; Abbrev mode, source: https://pragmaticemacs.com/emacs/use-abbreviations-to-expand-text (source not working).
(setq abbrev-file-name (concat emacs-init-dir "abbrev_defs.el"))
(add-hook 'text-mode-hook (lambda () (abbrev-mode t)))
(if (file-exists-p abbrev-file-name)
    (quietly-read-abbrev-file))


;; If files are .asc check if it's an addition to other mode extension
;; https://emacs.stackexchange.com/questions/13283/how-can-i-make-emacs-ignore-part-of-the-file-name-when-deciding-major-mode
(add-to-list 'auto-mode-alist '("\\.asc\\(~\\|\\.~[0-9]+~\\)?\\'" nil ascii-file))


;; File name completion;
;; http://endlessparentheses.com/improving-emacs-file-name-completion.html
(setq read-file-name-completion-ignore-case t)
(setq read-buffer-completion-ignore-case t)
(mapc (lambda (x)
        (add-to-list 'completion-ignored-extensions x))
      '(".a" ".acn" ".acr" ".alg" ".app" ".asv" ".aux" ".auxlock" ".bak" ".bbl" ".bcf" ".blg" ".brf" ".cab" ".cpt" ".dll" ".dpth" ".dvi" ".dylib" ".egg" ".elc" ".elf" ".end" ".epub" ".exe" ".fdb_latexmk" ".fff" ".fls" ".fmt" ".gch" ".glg" ".glo" ".gls" ".hex" ".idx" ".ilg" ".ind" ".ist" ".ko" ".la" ".lai" ".lib" ".lnk" ".lo" ".loa" ".lof" ".log" ".lol" ".lot" ".m~" ".maf" ".manifest" ".md5" ".mo" ".mobi" ".mod" ".msi" ".msm" ".msp" ".mtc" ".mw" ".nav" ".nlo" ".o" ".obj" ".out" ".pch" ".pdf" ".pdfsync" ".pot" ".pyg" ".pytxcode" ".rel" ".run.xml" ".sagetex.py" ".sagetex.sage" ".sagetex.scmd" ".sav" ".slo" ".snm" ".so" ".sout" ".spec" ".sympy" ".synctex" ".synctex.gz" ".synctex.gz(busy)" ".tdo" ".thm" ".tikz" ".toc" ".ttt" ".upa" ".upb" ".vrb" ".x86_64" ".xdy" "-pkg.el" "-autoloads.el" "auto/"))


;; Show unfinished keybindings early: https://pragmaticemacs.wordpress.com/2015/11/26/show-unfinished-keystrokes-early
(setq echo-keystrokes 0.1)


;; Prettify symbols http://endlessparentheses.com/new-in-emacs-25-1-have-prettify-symbols-mode-reveal-the-symbol-at-point.html
(global-prettify-symbols-mode 1)
(setq prettify-symbols-unprettify-at-point 'right-edge)


;; Gracefully shutdown server https://www.emacswiki.org/emacs/EmacsAsDaemon
(defun server-shutdown ()
  "Save buffers, Quit, and Shutdown (kill) server"
  (interactive)
  (save-some-buffers)
  (when (require 'elpy nil 'noerror) (pyvenv-deactivate))
  (kill-emacs)
  )


;; EWW
(defalias 'gk-urls-external-browser 'browse-url-xdg-open)
(defun gk-browse-url (&rest args)
  "Prompt for whether or not to browse with EWW, if no browse
with external browser."
  (apply
   (if (y-or-n-p "Browse with EWW? ")
       'eww-browse-url
     #'gk-urls-external-browser)
   args))
(setq browse-url-browser-function #'gk-browse-url)
