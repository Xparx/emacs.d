(message (concat "Loading latex config from: " emacs-init-dir "latex_conf.el"))

(add-hook 'LaTeX-mode-hook 'flyspell-mode)
(add-hook 'latex-mode-hook 'flyspell-mode)
(add-hook 'LaTeX-mode-hook (lambda () (setq ispell-parser 'tex)))
(add-hook 'LaTeX-mode-hook 'LaTeX-math-mode)
(add-to-list 'auto-mode-alist '("\\.tex$" . LaTeX-mode))

;; Make emacs aware of auctex.
;; Do not query for master file. This can be done with =C-c_=.
;; More information can be found here:
;; https://www.gnu.org/software/auctex/manual/auctex/Multifile.html.
;; !!!!!!!!!!!!!!! Left out multiple things from my old configs until i know I need it..
;; auctex config (does it work? require does not work)
(setq TeX-auto-save t)
(setq TeX-parse-self t)
(setq-default TeX-master t)

;; (setq TeX-outline-extra
;;    '((".*\\\\begin{frame}\n\\|.*\\\\begin{frame}\\[.*\\]\\|.*\\\\begin{frame}.*{.*}\\|.*[       ]*\\\\frametitle\\b" 3)))

;; Reftex (built into emacs)
;; (require 'reftex)
(setq reftex-default-bibliography '("./refs.bib" "./bibliography.bib" "~/research/bibliography.bib"))
(setq reftex-plug-into-AUCTeX t)
(add-hook 'LaTeX-mode-hook 'turn-on-reftex)
(add-hook 'latex-mode-hook 'turn-on-reftex)

(setq reftex-enable-partial-scans t)
(setq reftex-save-parse-info t)
(setq reftex-use-multiple-selection-buffers t)
(add-hook 'bibtex-mode-hook
          (lambda ()
            (define-key bibtex-mode-map "\M-q" 'bibtex-fill-entry)))


(when (require 'company-math nil 'noerror)
  (defun my-latex-mode-setup ()
    (setq-local company-backends
                (delete-dups (cons 'company-files
                                   company-backends)))
    (setq-local company-backends
                (delete-dups (cons '(company-math-symbols-latex company-latex-commands company-math-symbols-unicode)
                                   company-backends))))

  (add-hook 'LaTeX-mode-hook 'my-latex-mode-setup)
  (add-hook 'latex-mode-hook 'my-latex-mode-setup))
