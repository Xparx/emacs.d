;; ;; Example code
;; ;; Ignore changes in this file with
;; ;; $ git update-index --assume-unchanged user.el
;; (setq user-full-name "User Name"
;;       user-mail-address "user@email.address")
;; Load my prefered theme if availible
;; (condition-case nil
;;     (add-hook 'after-init-hook (lambda () (load-theme '<THEME> t)))
;;   (error "no theme loaded"))
